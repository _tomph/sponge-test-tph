(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Content = exports.Content = function () {
    function Content(contentURL, onLoadedCallback) {
        var _this = this;

        _classCallCheck(this, Content);

        var objContent = {};

        /**
         * Get the JSON file
         */
        $.getJSON(contentURL, function (objResponse) {
            _this.objContent = objResponse;
            onLoadedCallback();
        });
    }

    _createClass(Content, [{
        key: "getItem",
        value: function getItem(intItem) {
            return this.objContent[intItem];
        }
    }, {
        key: "populateContent",


        /**
        * PopulateHandleBarContent	 
        * @param {string} handlebarTemplateID ID of script element containing Handlebar Template
        * @param {string} contentID Parent node within loaded JSON content
        * @param {string} htmlElementID html container where content is injected
        * @returns {void}
        */
        value: function populateContent(handlebarTemplateID, contentID, htmlElementID) {

            if ($(handlebarTemplateID).length) {
                var strContentSource = $(handlebarTemplateID).html();
                var resContentTemplate = Handlebars.compile(strContentSource);
                var strContentHTML = resContentTemplate(this.getItem(contentID));
                if (strContentHTML == undefined || strContentHTML == null) {
                    throw new Error("ERROR: JSON Content ID: " + contentID + " does not exist!");
                } else {

                    if ($(htmlElementID).length) {
                        $(htmlElementID).append(strContentHTML);
                    } else {
                        throw new Error("ERROR: HTML Element of ID: " + htmlElementID + " does not exist!");
                    }
                }
            } else {
                throw new Error("ERROR: Handlebar Template: " + handlebarTemplateID + " does not exist!");
            }
        }
    }]);

    return Content;
}();

},{}],2:[function(require,module,exports){
'use strict';

var links = document.querySelectorAll('ul.tabs li a');

for (var i = 0; i < links.length; i++) {
        links[i].onclick = function () {
                var target = this.getAttribute('href').replace('#', '');
                var sections = document.querySelectorAll('section.menu');

                for (var j = 0; j < sections.length; j++) {
                        sections[j].style.display = 'none';
                }

                document.getElementById(target).style.display = 'block';

                for (var k = 0; k < links.length; k++) {
                        links[k].removeAttribute('class');
                }

                this.setAttribute('class', 'active-tab');

                return false;
        };
};

},{}],3:[function(require,module,exports){
"use strict";

require("./lib/tabs");
require("./lib/content");
require("./spongetest");

},{"./lib/content":1,"./lib/tabs":2,"./spongetest":4}],4:[function(require,module,exports){
/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

=====================================================
 SPONGE UK DEVELOPER TEST
 Page-specific JS
=====================================================
*/

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.entry = exports.SpongeTest = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _content = require('./lib/content');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SpongeTest = exports.SpongeTest = function () {
    function SpongeTest() {
        _classCallCheck(this, SpongeTest);

        this.content = new _content.Content('./dist/content.json', this.init.bind(this));
    }

    _createClass(SpongeTest, [{
        key: 'init',
        value: function init() {
            console.log("content loaded! powering up...");

            this.addStars();
            this.addContent();
        }
    }, {
        key: 'addStars',
        value: function addStars() {
            Handlebars.registerHelper('difficulty', function (intStars) {
                var strHTMLStarsOut = '';

                for (var intStar = 0; intStar < intStars; intStar++) {
                    strHTMLStarsOut += '<i class="fa fa-star"></i>';
                }

                for (var intBlankStar = intStars; intBlankStar < 5; intBlankStar++) {
                    strHTMLStarsOut += '<i class="fa fa-star-o"></i>';
                }

                return strHTMLStarsOut;
            });
        }
    }, {
        key: 'addContent',
        value: function addContent() {
            this.content.populateContent('#header-template', 'header', '#header');
            this.content.populateContent('#task-template', 'tasks', '#tasks');
            this.content.populateContent('#content-template', 'content', '#content');
            this.content.populateContent('#documentation-template', 'docs', '#documentation');
            this.content.populateContent('#about-template', 'about', '#about');
            this.content.populateContent('#photo-template', 'about', '#photo');
            this.content.populateContent('#photo-template', 'about', '#photo');
            this.content.populateContent('#links-template', 'about', '#links');
        }
    }]);

    return SpongeTest;
}();

var entry = exports.entry = new SpongeTest();

},{"./lib/content":1}]},{},[3]);
