# Sponge UK Developer Test (Tom Platten-Higgins)

## Instructions

1. cd into project root
1. `npm i`
2. `gulp'
3. [Open the test in your browser](http://localhost:3000) - this should happen automatically 

### Notes

All tasks complete :)