/*
 =====================================================

   _____                                _    _ _  __
  / ____|                              | |  | | |/ /
 | (___  _ __   ___  _ __   __ _  ___  | |  | | ' /
  \___ \| '_ \ / _ \| '_ \ / _` |/ _ \ | |  | |  <
  ____) | |_) | (_) | | | | (_| |  __/ | |__| | . \
 |_____/| .__/ \___/|_| |_|\__, |\___|  \____/|_|\_\
        | |                 __/ |
        |_|                |___/

=====================================================
 SPONGE UK DEVELOPER TEST
 Page-specific JS
=====================================================
*/

'use strict';

import { Content } from "./lib/content";

export class SpongeTest {

    constructor() {
        this.content = new Content('./dist/content.json', this.init.bind(this));
    }

    init() {
        console.log("content loaded! powering up...");

        this.addStars();
        this.addContent();
    }

    addStars() {
        Handlebars.registerHelper('difficulty',
            intStars => {
                let strHTMLStarsOut = '';

                for (let intStar = 0; intStar < intStars; intStar++) {
                    strHTMLStarsOut += '<i class="fa fa-star"></i>';
                }

                for (let intBlankStar = intStars; intBlankStar < 5; intBlankStar++) {
                    strHTMLStarsOut += '<i class="fa fa-star-o"></i>';
                }

                return strHTMLStarsOut;
            }
        );
    }

    addContent()
    {
        this.content.populateContent('#header-template', 'header', '#header');
        this.content.populateContent('#task-template', 'tasks', '#tasks');
        this.content.populateContent('#content-template', 'content', '#content');
        this.content.populateContent('#documentation-template', 'docs', '#documentation');
        this.content.populateContent('#about-template', 'about', '#about');
        this.content.populateContent('#photo-template', 'about', '#photo');
        this.content.populateContent('#photo-template', 'about', '#photo');
        this.content.populateContent('#links-template', 'about', '#links');
    }
}

export let entry = new SpongeTest();
