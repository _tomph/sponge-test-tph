'use strict';
export class Content {

    constructor(contentURL, onLoadedCallback) {

        let objContent = {};

        /**
         * Get the JSON file
         */
        $.getJSON(contentURL, objResponse => {
            this.objContent = objResponse;
            onLoadedCallback();
        });

    }
    getItem(intItem) {
        return this.objContent[intItem];
    };

    /**
 	* PopulateHandleBarContent	 
	* @param {string} handlebarTemplateID ID of script element containing Handlebar Template
	* @param {string} contentID Parent node within loaded JSON content
	* @param {string} htmlElementID html container where content is injected
	* @returns {void}
	*/
    populateContent(handlebarTemplateID, contentID, htmlElementID) {

        if ($(handlebarTemplateID).length) {
            var strContentSource = $(handlebarTemplateID).html();
            var resContentTemplate = Handlebars.compile(strContentSource);
            var strContentHTML = resContentTemplate(this.getItem(contentID));
            if (strContentHTML == undefined || strContentHTML == null) {
                throw new Error("ERROR: JSON Content ID: " + contentID + " does not exist!");
            } else {

                if ($(htmlElementID).length) {
                    $(htmlElementID).append(strContentHTML);
                } else {
                    throw new Error("ERROR: HTML Element of ID: " + htmlElementID + " does not exist!");
                }
            }
        } else {
            throw new Error("ERROR: Handlebar Template: " + handlebarTemplateID + " does not exist!");
        }
    }
}