var links = document.querySelectorAll('ul.tabs li a');

for (var i = 0; i < links.length; i++) {
    links[i].onclick = function() {
        var target = this.getAttribute('href').replace('#', '');
        var sections = document.querySelectorAll('section.menu');

        for (var j = 0; j < sections.length; j++) {
            sections[j].style.display = 'none';
        }

        document.getElementById(target).style.display = 'block';

        for (var k = 0; k < links.length; k++) {
            links[k].removeAttribute('class');
        }

        this.setAttribute('class', 'active-tab');

        return false;
    }
};