const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const babelify = require('babelify');
const browserify = require('browserify')
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

gulp.task('browser-sync', function (done) {
  browserSync({
    server: {
      baseDir: "./"
    }
  });
  done();
});

gulp.task('bs-reload', function (done) {
  browserSync.reload();
  done();
});

gulp.task('style', function (done) {
  gulp.src(['app/scss/style.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(gulp.dest('dist/css/'))
    .pipe(browserSync.reload({ stream: true }));
  done();
});

gulp.task('content', function (done) {
  gulp.src(['app/data/content.json'])
    .pipe(gulp.dest('dist/'))
  done();
});

gulp.task('transpile', function(done) {
  browserify('app/js/main.js')
      .transform('babelify', {
          presets: ['es2015']
      })
      .bundle()
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(gulp.dest('dist/'));

      done();
});

gulp.task('build', gulp.series('style', 'content', 'transpile', 'bs-reload', function (done) {
  done();
}));

gulp.task('default', gulp.series('build', 'browser-sync', function (done) {
  gulp.watch("app/scss/**/*.scss", gulp.parallel('build'));
  gulp.watch("app/js/**/*.js", gulp.parallel('build'));
  done();
}));
